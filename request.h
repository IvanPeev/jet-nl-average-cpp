#ifndef REQUEST_H_
#define REQUEST_H_

#include <curl/curl.h>

#include <string>

class Request {
   private:
    static std::string readBuffer;
    static size_t writeCallback(void *contents, size_t size, size_t nmemb, void *userp);

   public:
    CURLcode curlRequest(std::string);
    std::string getBuffer();
};
#endif
