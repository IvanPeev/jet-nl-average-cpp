#include "request.h"

#include <curl/curl.h>

#include <iostream>

std::string Request::readBuffer;

size_t Request::writeCallback(void *contents, size_t size, size_t nmemb, void *userp) {
    ((std::string *)userp)->append((char *)contents, size * nmemb);
    return size * nmemb;
}

CURLcode Request::curlRequest(std::string url) {
    CURL *curl = curl_easy_init();
    if (curl) {
        CURLcode res;

        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeCallback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);

        if (res == 0) {
            return CURLE_OK;
        }
    }

    return CURLE_FAILED_INIT;
}

std::string Request::getBuffer() {
    return readBuffer;
}
