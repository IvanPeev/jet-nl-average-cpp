#include <curl/curl.h>
#include <curl/curlver.h>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include "html-parser.h"
#include "request.h"
#include "restaurant.h"

#define BASE_LINK "https://www.thuisbezorgd.nl"
#define POSTAL_CODE "2564"
#define CATEGORY "Sushi"
#define MAIN_LINK BASE_LINK "/eten-bestellen-" POSTAL_CODE

// checks to see if original string contains a substring
bool stringContains(std::string original, std::string comp) {
    return original.find(comp) != std::string::npos;
}

// strips whitespace in strings
// ex: '                Sample string              '
// result: 'Sample string'
std::string stringStrip(std::string original) {
    std::string newString;
    int i = 0;

    while (original[i] == ' ') i++;
    newString = original.substr(i);

    int j = strlen(newString.c_str()) - 1;
    while (newString[j] == ' ') j--;
    return newString.substr(0, j + 1);
}

// get page via http request
std::string getPage(std::string link) {
    std::cout << "- GET - (" << link << ")" << std::endl;
    Request request;
    CURLcode code = request.curlRequest(link);

    if (code == CURLE_FAILED_INIT) {
        std::cerr << "[Error]: failed to initialize request - (" << link << ")" << std::endl;
        exit(1);
    }

    return request.getBuffer();
}

// gets all restaurants from the main page
std::vector<Restaurant> getAllRestaurants(std::string mainPage) {
    std::vector<Restaurant> restaurants;
    Restaurant restaurant;
    HTMLParser *parser = new HTMLParser(mainPage.c_str());

    // get all restaurants
    while (parser->hasMore()) {
        std::vector<char> v = parser->next();
        std::string s(v.begin(), v.end());

        // if there is an open tag
        if (parser->isOpenTag(s)) {
            std::vector<char> openV = parser->next();
            std::string openS(openV.begin(), openV.end());
            std::vector<std::array<std::string, 2>> attrs = parser->attrs(openS);
            bool inRestaurantName = false;

            for (auto attr : attrs) {
                if (attr[0] == "class") {
                    // if the current class is the restaurant name
                    // assign the name
                    if (attr[1] == "restaurantname notranslate") {
                        std::vector<char> textV = parser->next();
                        std::string textS(textV.begin(), textV.end());

                        restaurant.name = stringStrip(textS);
                        inRestaurantName = true;
                    }

                    // if the current class is the kitchens
                    // assign the kitchens
                    if (attr[1] == "kitchens") {
                        std::vector<char> textV = parser->next();
                        std::string textS(textV.begin(), textV.end());
                        restaurant.kitchen = stringStrip(textS);
                    }
                }

                // if we are still in the restaurant name tag and the current attr is the link
                // assign the link
                if (inRestaurantName && attr[0] == "href") {
                    restaurant.link = BASE_LINK + attr[1];
                    inRestaurantName = false;
                }
            }

            // if all the necessary fields have been filled in
            // add the restaurant to the list
            if (!restaurant.name.empty() && !restaurant.link.empty() && !restaurant.kitchen.empty()) {
                restaurant.average = 0.00f;
                restaurants.push_back(restaurant);
                restaurant = Restaurant();
            }
        }
    }

    return restaurants;
}

// gets all restaurants by category
std::vector<Restaurant> getEntries(const std::string mainPage) {
    std::cout << "- PROC - matching by category" << std::endl;
    std::vector<Restaurant> restaurants = getAllRestaurants(mainPage);
    std::vector<int> indices;

    /**
     * erase all indices starting from the back to the front
     * this way the indices of the list don't shift
     * ex: [a, b, c, d, e]
     * to be removed are items at indices 1 and 3
     * by doing this forwards we get this:
     * [a, b, c, d, e] -> [a, c, d, e] -> [a, c, d]
     * the value at index 3 was supposed to be value d, and not value e
     * by removing backwards we get: [a, b, c, d, e] -> [a, b, c, e] -> [a, c, e]
     */
    for (int i = restaurants.size() - 1; i >= 0; i--) {
        if (!stringContains(restaurants[i].kitchen, CATEGORY)) {
            restaurants.erase(restaurants.begin() + i);
        }
    }

    return restaurants;
}

float getPageAverage(std::string pageLink) {
    std::string page = getPage(pageLink);
    HTMLParser *parser = new HTMLParser(page.c_str());
    float total;
    size_t len = 0;

    // sum up all prices in the restaurant
    while (parser->hasMore()) {
        std::vector<char> v = parser->next();
        std::string s(v.begin(), v.end());
        std::vector<std::array<std::string, 2>> attrs = parser->attrs(s);

        for (auto attr : attrs) {
            if (attr[0] == "class" && attr[1] == "meal__price notranslate") {
                std::vector<char> priceV = parser->next();
                std::string priceS(priceV.begin(), priceV.end());
                priceS = stringStrip(priceS).substr(4);

                std::replace(priceS.begin(), priceS.end(), ',', '.');
                total += std::stof(priceS);
                len++;
            }
        }
    }

    return (total / len);
}

bool compareByAverage(const Restaurant &a, const Restaurant &b) {
    return a.average < b.average;
}

void displayTopRestaurants(std::vector<Restaurant> restaurants, int limit = 5) {
    std::cout << "\n";
    for (int i = 0; i < limit; i++) {
        std::cout << restaurants[i].name << "\t: €" << restaurants[i].average << std::endl;
    }
}

int main() {
    std::cout << "- PARAMS - (" << POSTAL_CODE << ", " << CATEGORY << ")" << std::endl;
    std::string mainPage = getPage(MAIN_LINK);
    std::vector<Restaurant> restaurants = getEntries(mainPage);

    for (int i = 0; i < restaurants.size(); i++) {
        restaurants[i].average = getPageAverage(restaurants[i].link);
    }

    std::cout << "- PROC - sorting averages" << std::endl;
    std::sort(restaurants.begin(), restaurants.end(), compareByAverage);

    std::cout << "- DONE - displaying top 5 cheapest restaurants on average" << std::endl;
    displayTopRestaurants(restaurants);

    return 0;
}
