#ifndef HTML_PARSER_H_
#define HTML_PARSER_H_

#define TAG_TYPE_NONE 0;    // not a tag
#define TAG_TYPE_TAG 1;     // is a tag
#define TAG_TYPE_OPEN 3;    // is a tag and is an open tag
#define TAG_TYPE_CLOSED 5;  // is a tag and is a closed tag
#define TAG_TYPE_FULL 9;    // is a tag and is a full tag

#include <stdlib.h>
#include <string.h>

#include <array>
#include <queue>
#include <stack>
#include <string>
#include <vector>

class HTMLParser {
   private:
    const char *raw_html;
    int raw_html_len;

    // the current index to the raw_html
    int _i;

    // stack of previous indices
    std::stack<int> prevIndices;

    // stack of indicies that we already used
    std::stack<int> forwardIndices;

   public:
    HTMLParser(const std::string);
    HTMLParser(const char *);

    void rewind(int);
    void fastForward(int);

    std::vector<char> valueString(const char *, const char *) const;
    std::vector<char> valueString(const char *, const std::string) const;
    std::vector<char> valueString(const std::string, const char *) const;
    std::vector<char> valueString(const std::string, const std::string) const;

    std::vector<char> tagname(const char *) const;
    std::vector<char> tagname(const std::string) const;

    std::vector<char> id(const char *) const;
    std::vector<char> id(const std::string) const;

    std::vector<std::vector<char> > classes(const char *) const;
    std::vector<std::vector<char> > classes(const std::string) const;

    // return a vector of arrays where element 0 is attr name and 1 the value
    std::vector<std::array<std::string, 2> > attrs(const char *) const;
    std::vector<std::array<std::string, 2> > attrs(const std::string) const;

    int cmpTags(const char *, const char *) const;
    int cmpTags(const std::string, const std::string) const;

    std::vector<char> nextTag();
    std::vector<char> next();

    signed char getTagType(const char *) const;
    signed char getTagType(const std::string) const;

    bool isOpenTag(const char *) const;
    bool isOpenTag(const std::string) const;

    bool isCloseTag(const char *) const;
    bool isCloseTag(const std::string) const;

    bool isTag(const char *) const;
    bool isTag(const std::string) const;

    bool isFullTag(const char *) const;
    bool isFullTag(const std::string) const;

    bool hasMore() const;

    bool isEscapeSequence(const char) const;

    const char *getRawHTML() const;
    int getCurrentIndex() const;
    int getRawHTMLLength() const;

    void setRawHTML(const char *);
    void setCurrentIndex(int);
    void setRawHTMLLength(int);

    static const signed char tag_type_none = TAG_TYPE_NONE;
    static const signed char tag_type_tag = TAG_TYPE_TAG;
    static const signed char tag_type_open = TAG_TYPE_OPEN;
    static const signed char tag_type_closed = TAG_TYPE_CLOSED;
    static const signed char tag_type_full = TAG_TYPE_FULL;
};

#endif
