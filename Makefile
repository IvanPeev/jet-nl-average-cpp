CC=g++
CXXFLAGS=-Wall -lcurl

program: average-out request-out html-parser-out
	$(CC) -o average average.o request.o html-parser.o $(CXXFLAGS)

average-out: average.cpp request.h html-parser.h
	$(CC) -c average.cpp

request-out: request.cpp request.h
	$(CC) -c request.cpp

html-parser-out: html-parser.cpp html-parser.h
	$(CC) -c html-parser.cpp

clean:
	rm -f *.o average
.PHONY: clean
