#include <string>

struct Restaurant {
    std::string name;
    std::string link;
    std::string kitchen;
    float average;
};